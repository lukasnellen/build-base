#! /usr/bin/env python3

import json
import subprocess
import sys
import re

connection_var = {"ansible_connection": "buildah"}

container_res = subprocess.run(["buildah", "ls", "--json"], capture_output=True, text=True)
container_data = json.loads(container_res.stdout)

need_py3 = re.compile(r".*:(d-.*|u-2.\..*|a|gcc-.*)$")

ungrouped = []
devtoolset = []
stream = []
gcc = []
containers = []

if container_data is not None:
    for c in container_data:
        container = c["containername"]
        grouped = False
        containers.append(container)
        if not container.startswith("sysdev:"):
            continue
        if container.endswith("scl"):
            devtoolset.append(container)
            grouped = True
        if container.endswith("stream"):
            stream.append(container)
            grouped = True
        if container.startswith("sysdev:gcc"):
            gcc.append(container)
            grouped = True
        if not grouped:
            ungrouped.append(container)

inventory = {
    "_meta": {
        "hostvars": dict([(c, connection_var.copy()) for c in ungrouped + devtoolset + stream + gcc])
    },
    "all": {
        "children": [
            "ungrouped"
        ]
    }
}

for h in containers:
    if need_py3.match(h) is not None:
        inventory["_meta"]["hostvars"][h]["ansible_python_interpreter"] = "/usr/bin/python3"

for h in gcc:
    inventory["_meta"]["hostvars"][h]["gcc_preinstalled"] = "true"

    
if ungrouped:
    inventory["ungrouped"] = {"hosts": ungrouped}
        

if devtoolset:
    inventory["all"]["children"].append("devtoolset")
    inventory["devtoolset"] = devtoolset

if stream:
    inventory["all"]["children"].append("stream")
    inventory["stream"] = stream

if gcc:
    inventory["all"]["children"].append("gcc")
    inventory["gcc"] = gcc


if len(sys.argv) > 1 and sys.argv[1] == "--list":
    print(json.dumps(inventory))
elif len(sys.argv) > 2 and sys.argv[1] == "--host" and sys.argv[2] in ungrouped + devtoolset:
    print(json.dumps(inventory["_meta"]["hostvars"][sys.argv[2]]))
else:
    sys.exit(1)
