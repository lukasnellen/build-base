Install build tools in containers and machines
==============================================

The tools are mainly ansible based, with some support scripts.

Physical and virtual machines
-----------------------------

The `setup.yml` and, if needed, `scl.yml` playbooks manage the configuration and installation of the required tools.
You just have to provide the inventory to point to the machines you want to set up.

Containers
----------

For containers, this uses buildah and podman as the basic tools. Containers can be
converted to docker or singularity.

The `sysdev-prepare` script sets up the buildah containers. The `sysdev-install` script
runs the playbooks for installation.

The script `buildah-commit` triggers some cleanup in the containers and commits, turning 
them into a podman image. This image can be converted docker format using the `docker push` 
command to send a copy to the docker hub or the local daemon.

### Container naming

The `sysdev` containers contain the build tools needed.

Convention for tags or suffixes: One or two letters to indicate the distribution,
followed by a dash, version number or other version indicator. Can be followed by
other indicators.

- c: centos
- f: fedora
- u: ubuntu
- d: debian
- a: arch linux (no further suffix)

Examples: `sysdev:d-10`, `sysdev:c-7`. Special cases can be:

- `sysdev:c-7scl`: centos 7 with extra compilers from softwarecollections.org
- `sysdev:d-testing`: debian testing (no version number)

