#! /bin/bash

#set -x
set -e

CENTOS=(stream8 stream9)
CENTOS_SCL=(7)

FEDORA=(34 35 36) # rawhide)

ALMA=(8 9)
ROCKY=(8)

DEBIAN=(10 11) # testing)

UBUNTU=(16.04 18.04 20.04 21.10 22.04)

containers=$(mktemp) || exit
trap "rm -f -- '${containers}'" EXIT
buildah ls >>${containers}

exists()
{
    egrep -q $1\$ ${containers}
}

python_apt()
{
    buildah run $1 rm -f etc/apt/apt.conf.d/docker-clean
    buildah run $1 apt-get update
    buildah run $1 ln -snf /usr/share/zoneinfo/America/Mexico_City /etc/localtime
    buildah run $1 echo /etc/timezoneecho "America/Mexico_city" '> /etc/timezone'
    buildah run $1 apt-get install --yes --no-install-recommends python3
}

python_pacman()
{
    buildah run $1 pacman --noconfirm --noprogressbar -Syu python3
}

rh()
{
    name=sysdev:$2-$3$4
    exists ${name} && return
    buildah from --security-opt=seccomp=unconfined --name ${name} $1:$3
}

debian()
{
    name=sysdev:d-$1
    exists ${name} && return
    buildah from --name ${name} debian:$1
    python_apt ${name}
}

gcc()
{
    name=sysdev:gcc-$1
    exists ${name} && return
    buildah from --name ${name} gcc:$1
    python_apt ${name}
}

ubuntu()
{
    name=sysdev:u-$1
    exists ${name} && return
    buildah from --name ${name} ubuntu:$1
    python_apt ${name}
}

arch()
{
    name=sysdev:a
    exists ${name} && return
    buildah from --name ${name} archlinux
    python_pacman ${name}
}

prepare_all()
{
    for i in ${CENTOS[@]}; do rh centos c $i; done
    for i in ${CENTOS_SCL[@]}; do rh centos c $i scl; done
    for i in ${FEDORA[@]}; do rh fedora f $i; done
    for i in ${ALMA[@]}; do rh rockylinux r $i; done
    for i in ${ROCKY[@]}; do rh almalinux al $i; done
    for i in ${DEBIAN[@]}; do debian $i; done
    for i in ${UBUNTU[@]}; do ubuntu $i; done
    arch
}

prepare_one()
{
    code=$1
    shift;
    case ${code} in
	c) rh centos c $@ ;;
	f) rh fedora f $@ ;;
	r) rh rockylinux r $@ ;;
	al) rh almalinux al $@ ;;
	d) debian $@ ;;
	u) ubuntu $@ ;;
	a) arch ;;
	gcc) gcc $@ ;;
    esac
}

if [ -n "$1" ]; then
    prepare_one $@
else
    prepare_all
fi

rm -f -- "${containers}"
trap - EXIT
